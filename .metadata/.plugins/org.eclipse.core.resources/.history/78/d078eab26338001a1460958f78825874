package be.helha.aemt.control;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.Part;

import com.mysql.fabric.xmlrpc.base.Array;

import be.helha.aemt.ejb.ManagementOfferEJB;
import be.helha.aemt.entities.AlumnusUser;
import be.helha.aemt.entities.InternshipOffer;
import be.helha.aemt.entities.JobOffer;
import be.helha.aemt.entities.Offer;
import be.helha.aemt.entities.Section;

@SessionScoped
@Named
public class OfferControl implements Serializable {
	
	private String selectedCategory;
	private String selectedOption = "Internship";
	
	private String name;
	private String section;
	private String description;
	private String date;
	private String place;
	
	//InternshipOffer
	private int last;
	private boolean canStudent;
	
	//JobOffer
	private int experience;
	private String type;
	private double salary;
	
	private List<Offer> list;
	
	private Part file;

	@EJB
	private ManagementOfferEJB beanOffer;


	public String getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(String selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public String getSelectedOption() {
		return selectedOption;
	}

	public void setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}
	
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}
	
	public List<Offer> getList() {
		return list;
	}

	public void setList(List<Offer> list) {
		this.list = list;
	}
	
	@PostConstruct
	public void init() {
		section = Section.DEFAULT.toString();
	}

	public String saveOffer() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dateObject = new Date();
		this.date = dateFormat.format(dateObject);
		section = selectedCategory;
		if(selectedOption.equals("Internship")) {
			InternshipOffer internshipOffer = new InternshipOffer(name, section, description, date, place, false, last, canStudent);
			beanOffer.add(internshipOffer);
		} else if (selectedOption.equals("Job")) {
			JobOffer jobOffer = new JobOffer(name, section, description, date, place, false, salary, type, experience);
			beanOffer.add(jobOffer);
		}
		
		return "offres.xhtml";
	}
	
	public String doComplementaryInformation() {
		return "moreInformation" + selectedOption + ".xhtml";
	}

	public int getLast() {
		return last;
	}

	public void setLast(int last) {
		this.last = last;
	}

	public boolean isCanStudent() {
		return canStudent;
	}

	public void setCanStudent(boolean canStudent) {
		this.canStudent = canStudent;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}

	public int countJobOffers() {
		return beanOffer.countJobOffers();
	}
	
	public int countInternshipOffers() {
		return beanOffer.countInternshipOffers();
	}
	
	public void doSelectAllOffers() {
		list = new ArrayList<Offer>();
		list.addAll(beanOffer.selectAllOffers());
	}
	
	public void selectNotValidated() {
		list = new ArrayList<>();
		list.addAll(beanOffer.selectNotValidated());
	}
	
	public void selectValidated() {
		list = new ArrayList<>();
		list.addAll(beanOffer.setValidated());
	}
	
	public void save() {
		try (InputStream input = this.file.getInputStream()) {
			File fileCreator = new File("." + File.separator + "uploads" + File.separator + "A");
			fileCreator.getParentFile().mkdir();
        	Files.copy(input, new File("." + File.separator + "uploads" + File.separator + name + "_Alumni").toPath());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Offer retrieveOfferWithName(String name) {
		for(Offer offer : list) {
			if(offer.getName().equals(name)) {
				return offer;
			}
		}
		return null;
	}
	
	public String doUpdateValidation(String name) {
		Offer offer = retrieveOfferWithName(name);
		offer.setValidated(true);
		beanOffer.updateOfferValidated(offer);
		return "validationOffers.xhtml";
	}
	
	public String doDeleteValidation(String name) {
		Offer offer = retrieveOfferWithName(name);
		beanOffer.deleteOfferValidated(offer);
		return "validationOffers.xhtml";
	}
	
	public String doDeleteOffer(String name) {
		Offer offer = retrieveOfferWithName(name);
		beanOffer.deleteOffer(offer);
		return "manageOffers.xhtml";
	}
	
	public int countValidations() {
		return beanOffer.selectCountValidations();
	}
	
	public String emptyTableMessage() {
		if(list.isEmpty())
		{
			return "Aucune offre � afficher";
		}
		else
		{
			return "";
		}
	}
	
	////////////////////////////////////////////////////////////
	// PDF
	////////////////////////////////////////////////////////////
	
	
	
}
