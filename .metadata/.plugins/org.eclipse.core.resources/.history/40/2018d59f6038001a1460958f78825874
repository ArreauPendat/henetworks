package be.helha.aemt.control;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import be.helha.aemt.ejb.ManagementUserEJB;
import be.helha.aemt.entities.AlumnusUser;
import be.helha.aemt.entities.Group;
import be.helha.aemt.entities.Section;
import be.helha.aemt.entities.User;
import be.helha.aemt.util.Sha256Utility;

@SessionScoped
@Named
public class UserControl implements Serializable {
	
	private String firstName;
	private String lastName;
	private String description;
	private String mail;
	private boolean wantNotification;
	private int graduatedYear;
	private String phoneNumber;
	private boolean isValidated = false;
	private String password;
	private String section = Section.DEFAULT.toString();
	private String groupName = Group.VISITOR.toString();
	private boolean hasAgreed = false;
	
	private boolean canDisplayed;
	private String testimony;
	private boolean hasBeenAcceptedForDisplay;

	private List<AlumnusUser> list;
	
	private String filterFirstName="";
	private String filterLastName="";

	@EJB
	private ManagementUserEJB managementUserEJB;
	
	////////////////////////////////////////////////////////////////////
	// NAVIGATION
	////////////////////////////////////////////////////////////////////
	
	public String doConnection() {
		return "login.xhtml";
	}
	
	public String doCreateAccount() {
		return "createAccount.xhtml";
	}
	
	public String logout() {
	    HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	    session.invalidate();
	    return doConnection();
	}
	
	public String doIndex() {
		return "index.xhtml";
	}
	
	public boolean isAdmin() {
		return groupName.equals(Group.ADMINISTRATOR.toString());
	}

	////////////////////////////////////////////////////////////////////
	// GETTERS AND SETTERS
	////////////////////////////////////////////////////////////////////

	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public boolean isWantNotification() {
		return wantNotification;
	}

	public void setWantNotification(boolean wantNotification) {
		this.wantNotification = wantNotification;
	}

	public int getGraduatedYear() {
		return graduatedYear;
	}

	public void setGraduatedYear(int graduatedYear) {
		this.graduatedYear = graduatedYear;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isValidated() {
		return isValidated;
	}

	public void setValidated(boolean isValidated) {
		this.isValidated = isValidated;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean isHasAgreed() {
		return hasAgreed;
	}

	public void setHasAgreed(boolean isAgreeing) {
		this.hasAgreed = isAgreeing;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public List<AlumnusUser> getList() {
		return list;
	}
	
	public void setList(List<AlumnusUser> list) {
		this.list = list;
	}
	
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}
	
	@PostConstruct
	public void init() {
		section = Section.DEFAULT.toString();
	}
	
	public String getFilterFirstName() {
		return filterFirstName;
	}

	public void setFilterFirstName(String filterFirstName) {
		this.filterFirstName = filterFirstName;
	}

	public String getFilterLastName() {
		return filterLastName;
	}

	public void setFilterLastName(String filterLastName) {
		this.filterLastName = filterLastName;
	}
	
	public boolean isCanDisplayed() {
		return canDisplayed;
	}

	public void setCanDisplayed(boolean canDisplayed) {
		this.canDisplayed = canDisplayed;
	}

	public String getTestimony() {
		return testimony;
	}

	public void setTestimony(String testimony) {
		this.testimony = testimony;
	}

	public boolean isHasBeenAcceptedForDisplay() {
		return hasBeenAcceptedForDisplay;
	}

	public void setHasBeenAcceptedForDisplay(boolean hasBeenAcceptedForDisplay) {
		this.hasBeenAcceptedForDisplay = hasBeenAcceptedForDisplay;
	}

	////////////////////////////////////////////////////////////////////
	// METHODS
	////////////////////////////////////////////////////////////////////
	public User createUser() {
		AlumnusUser alumnus = new AlumnusUser(
				mail,
				Sha256Utility.convertToSha256( ((password == null || password.equals(""))?"":password) ),
				firstName,
				lastName,
				description,
				"pic",
				groupName,
				section,
				phoneNumber,
				wantNotification,
				graduatedYear,
				isValidated,
				canDisplayed,
				testimony);
		return alumnus;
	}
	
	public boolean persistUser() {
		return managementUserEJB.persistUser(createUser());
		//TODO reset le formulaire et changer de page
	}
	
	public void doSelectAllNotValidated() {
		list = new ArrayList<AlumnusUser>();
		list.addAll(managementUserEJB.selectAllNotValidated());
	}
	
	public void doSelectAllValidated() {
		list = new ArrayList<AlumnusUser>();
		list.addAll(managementUserEJB.selectAllValidated());
	}
	
	public String getSessionMail() {
		if(FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal() != null)
		{
			mail = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
		}
		else
		{
			mail = null;
		}
		return mail;
	}
		
	public void populateWithConnectedUser() {
		User user = managementUserEJB.selectByMail(getSessionMail());
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.description = user.getDescription();
		this.groupName = user.getGroupName();
		if( user.getGroupName().equals(Group.ALUMNUS.toString()) || user.getGroupName().equals(Group.ADMINISTRATOR.toString()) ) {
			this.wantNotification = ((AlumnusUser) user).isWantNotification();
			this.graduatedYear = ((AlumnusUser) user).getGraduatedYear();
			this.phoneNumber = ((AlumnusUser) user).getPhoneNumber();
			this.isValidated = ((AlumnusUser) user).isIsValidated();
			this.canDisplayed = ((AlumnusUser) user).isCanDisplayed();
			this.testimony = ((AlumnusUser) user).getTestimony();
		}
	}
	
	public User getCurrentlyConnected() {
		populateWithConnectedUser();
		return createUser();
	}
	
	public String validationMessage() {
		if(groupName.equals(Group.ADMINISTRATOR.toString()))
		{
			return "Votre compte est un compte administrateur";
		}
		else
		{
			if(!isValidated) {
				return "Votre compte est en attente de validation par un administrateur.";
			}
			return "Votre compte a �t� v�rifi� et valid� par un administrateur!";
		}
	}
	
	public String saveInformation() {
		password = "password"; // Initilisation pour �viter les erreurs, le mot de passe n'est pas r�ellement chang� en soit
		managementUserEJB.updateAlumnus( (AlumnusUser) createUser());
		return "profile.xhtml";
	}
	
	public AlumnusUser retrieveAlumnusWithMail(String mail) {
		for(AlumnusUser alum : list) {
			if(alum.getMail().equals(mail)) {
				return alum;
			}
		}
		return null;
	}
	
	public String doUpdateValidation(String mail) {
		AlumnusUser alumnus = retrieveAlumnusWithMail(mail);
		alumnus.setIsValidated(true);
		managementUserEJB.updateAlumnusValidated(alumnus);
		return "validationInscriptions.xhtml";
	}
	
	public String doDeleteUser(String mail) {
		AlumnusUser alumnus = retrieveAlumnusWithMail(mail);
		managementUserEJB.deleteAlumnus(alumnus);
		return "manageUsers.xhtml";
	}
	
	public int countMembers() {
		return managementUserEJB.selectCountMembers();
	}
	
	public int countValidations() {
		return managementUserEJB.selectCountValidations();
	}
	
	public String emptyTableMessage() {
		if(list.isEmpty())
		{
			return "Aucune inscription � afficher";
		}
		else
		{
			return "";
		}
	}
	

	/*
	public String doFilterByFirstName() {
		
		return "alumniDirectory.xhtml";
	}
	
	public void loadList() {
		if(filterFirstName.equals("") && filterLastName.equals("")) {
			doSelectAllValidated();
		}
		else if(filterFirstName.equals(""))
		{
			doFilterByFirstName();
		}
	}*/

	public void createPDF() {
		
		String filePath = "." + File.separator + "profiles_pdf" + File.separator + lastName + "_" + firstName + "_Alumni.pdf";
		Document document = new Document();
		try {
			File file = new File(filePath);
			file.getParentFile().mkdir();
			FileOutputStream output = new FileOutputStream(file);
			PdfWriter.getInstance(document, output);
			document.open();
			Font font = FontFactory.getFont(FontFactory.TIMES, 16, BaseColor.BLUE);
			Chunk mainProfile = new Chunk("Profil d'utilisateur", font);
			
			Phrase phrase1 = new Phrase();
			phrase1.add("Nom : " + lastName + "\nPrenom : " + firstName);
			
			Phrase phrase2 = new Phrase();
			phrase2.add("\nSection : " + section + "\nDiplom� en " + graduatedYear);
			
			Phrase phrase3 = new Phrase();
			phrase3.add("\n\n" + description + "\n\n");
			
			Phrase phrase4 = new Phrase();
			
			
			mainProfile.setUnderline(5.0f, 150.0f);
			mainProfile.append(phrase1.toString() + "\n" + phrase2.toString() + "\n" + phrase3.toString());
			
			
			
			document.add(mainProfile);
			
			document.close();
			output.close();
		} catch (FileNotFoundException | DocumentException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void downloadPDF() throws IOException {
		createPDF();
		
		FileInputStream file = new FileInputStream("." + File.separator + "profiles_pdf" + File.separator + lastName + "_" + firstName + "_Alumni.pdf"); 	    
		
		FacesContext fc = FacesContext.getCurrentInstance();
	    ExternalContext ec = fc.getExternalContext();

	    ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
	    ec.setResponseContentType("application/pdf"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
	    ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + (lastName + "_" + firstName + "_Alumni.pdf") + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.

	    OutputStream output = ec.getResponseOutputStream();
	    // Now you can write the InputStream of the file to the above OutputStream the usual way.
	    // ...
	    byte[] buffer = new byte[1024];
        int length;
        while ((length = file.read(buffer)) > 0) {
            output.write(buffer, 0, length);
        }

	    fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.

	}
	

}
