package be.helha.aemt.entities;

public enum Role {
	TEACHER ("Teacher"),
	DIRECTION ("Direction");
	   
	private String name;
		   
	Role(String name){
		this.name = name;
	}
	   
	public String toString(){
		return name;
	}
}
