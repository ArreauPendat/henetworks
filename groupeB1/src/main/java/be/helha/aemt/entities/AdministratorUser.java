package be.helha.aemt.entities;

import javax.persistence.Entity;

@Entity
public class AdministratorUser extends User{

	public AdministratorUser() {
		// TODO Auto-generated constructor stub
	}
	
	public AdministratorUser(String mail, String password, String firstName, String lastName, String description,
			String pictureLink, String groupName) {
		super(mail, password, firstName, lastName, description, pictureLink, groupName);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "AdministratorUser [getFirstName()=" + getFirstName() + ", getLastName()=" + getLastName()
				+ ", getDescription()=" + getDescription() + ", getPictureLink()=" + getPictureLink()
				+ ", getGroupName()=" + getGroupName() + ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}
}
