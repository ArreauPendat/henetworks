package be.helha.aemt.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String street;
	private String number;
	private int postalCode;
	private String town;
		
	public Address() {};
	
	public Address(String street, String number, int postalCode, String town) {
		super();
		this.street = street;
		this.number = number;
		this.postalCode = postalCode;
		this.town = town;
	}
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	
	@Override
	public String toString() {
		return "Address [street=" + street + ", number=" + number + ", postalCode=" + postalCode + ", town=" + town
				+ "]";
	}
}
