package be.helha.aemt.entities;

public class Mail {
	private String sender;
	private String receiver;
	private String bodyText;
	
	public Mail(String sender, String receiver, String bodyText) {
		super();
		this.sender = sender;
		this.receiver = receiver;
		this.bodyText = bodyText;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getBodyText() {
		return bodyText;
	}

	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}

	@Override
	public String toString() {
		return "Mail [sender=" + sender + ", receiver=" + receiver + ", bodyText=" + bodyText + "]";
	}	
}
