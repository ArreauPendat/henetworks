package be.helha.aemt.entities;

import javax.persistence.Entity;

@Entity
public class InternshipOffer extends Offer{
	private int last;
	private boolean canStudent;
	
	public InternshipOffer() {}
	
	public InternshipOffer(String name, String section, String description, String date, String place, boolean validated, int last, boolean canStudent) {
		super(name, section, description, date, place, validated);
		this.last = last;
		this.canStudent = canStudent;
	}

	public int getLast() {
		return last;
	}

	public void setLast(int last) {
		this.last = last;
	}

	public boolean isCanStudent() {
		return canStudent;
	}

	public void setCanStudent(boolean canStudent) {
		this.canStudent = canStudent;
	}

	@Override
	public String toString() {
		return "InternshipOffer [last=" + last + ", canStudent=" + canStudent + ", getName()=" + getName()
				+ ", getDescription()=" + getDescription() + ", getDate()=" + getDate() + ", getPlace()=" + getPlace()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}
}
