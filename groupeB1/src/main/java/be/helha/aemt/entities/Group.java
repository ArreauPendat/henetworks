package be.helha.aemt.entities;

public enum Group {
	ADMINISTRATOR ("admin"),
	ALUMNUS ("ancien"),
	VISITOR ("visitor");
	   
	private String name;
		   
	Group(String name){
		this.name = name;
	}
	   
	public String toString(){
		return name;
	}
}
