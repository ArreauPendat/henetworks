package be.helha.aemt.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Announce {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String title;
	private String datePosted;
	private String text;
	
	public Announce() {}
	
	public Announce(String title, String text) {
		this.title = title;
		this.text = text;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date dateAjd = new Date();
		this.datePosted = format.format(dateAjd);
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDatePosted() {
		return datePosted;
	}
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Announce [title=" + title + ", datePosted=" + datePosted + "]";
	}
}
