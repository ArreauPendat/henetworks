package be.helha.aemt.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Event {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String startDate;
	private String endDate;
	private int placesAvailable;
	private String name;
	private String description;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Address address;
	
	public Event() {};
	
	public Event(String startDate, String endDate, int placesAvailable, String name, String description, Address address) throws ParseException {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.placesAvailable = placesAvailable;
		this.name = name;
		this.description = description;
		this.address = address;
	}
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getPlacesAvailable() {
		return placesAvailable;
	}
	public void setPlacesAvailable(int placesAvailable) {
		this.placesAvailable = placesAvailable;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Event [startDate=" + startDate + ", endDate=" + endDate + ", placesAvailable=" + placesAvailable
				+ ", name=" + name + ", description=" + description + "]";
	}	
}
