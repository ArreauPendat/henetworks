package be.helha.aemt.entities;

public enum Section {
	DEFAULT ("Non fournie"),
	ACCOUNTING ("Comptabilité"),
	BUSINESS_COMPUTING ("Informatique de gestion"),
	EXECUTIVE_ASSISTANT ("Assistant de direction");
	   
	private String name;
		   
	Section(String name){
		this.name = name;
	}
	   
	public String toString(){
		return name;
	}
}
