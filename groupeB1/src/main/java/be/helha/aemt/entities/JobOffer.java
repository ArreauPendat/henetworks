package be.helha.aemt.entities;

import javax.persistence.Entity;

@Entity
public class JobOffer extends Offer{
	
	private double salary;
	private String type;
	private int experience;
	
	public JobOffer() {}
	
	public JobOffer(String name, String section, String description, String date, String place, boolean validated, double salary, String type,
			int experience) {
		super(name, section, description, date, place, validated);
		this.salary = salary;
		this.type = type;
		this.experience = experience;
	}
	
	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	@Override
	public String toString() {
		return "JobOffer [salary=" + salary + ", type=" + type + ", experience=" + experience + ", getName()="
				+ getName() + ", getDescription()=" + getDescription() + ", getDate()=" + getDate() + ", getPlace()="
				+ getPlace() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}
}
