package be.helha.aemt.entities;

import javax.persistence.Entity;

@Entity
public class AlumnusUser extends User{
	private String phoneNumber;
	private boolean wantNotification;
	private int graduatedYear;
	private String section;
	private boolean isValidated;
	
	private boolean canDisplayed;
	private String testimony;
	
	private boolean hasBeenValidatedForDisplay;
	
	public AlumnusUser() {
		// TODO Auto-generated constructor stub
	}

	

	public AlumnusUser(String mail, String password, String firstName, String lastName, String description,
			String pictureLink, String groupName, String section, String phoneNumber, boolean wantNotification, int graduatedYear,
			boolean isValidated, boolean canDisplayed, String testimony) {
		super(mail, password, firstName, lastName, description, pictureLink, groupName);
		this.section = section;
		this.phoneNumber = phoneNumber;
		this.wantNotification = wantNotification;
		this.graduatedYear = graduatedYear;
		this.isValidated = isValidated;
		this.canDisplayed = canDisplayed;
		this.testimony = testimony;
		
		this.hasBeenValidatedForDisplay = false;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isWantNotification() {
		return wantNotification;
	}

	public void setWantNotification(boolean wantNotification) {
		this.wantNotification = wantNotification;
	}

	public int getGraduatedYear() {
		return graduatedYear;
	}

	public void setGraduatedYear(int graduatedYear) {
		this.graduatedYear = graduatedYear;
	}

	public boolean isIsValidated() {
		return isValidated;
	}

	public void setIsValidated(boolean isValidated) {
		this.isValidated = isValidated;
	}
	
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}
	
	public boolean isCanDisplayed() {
		return canDisplayed;
	}

	public void setCanDisplayed(boolean canDisplayed) {
		this.canDisplayed = canDisplayed;
	}

	public String getTestimony() {
		return testimony;
	}

	public void setTestimony(String testimony) {
		this.testimony = testimony;
	}

	public boolean isHasBeenValidatedForDisplay() {
		return hasBeenValidatedForDisplay;
	}

	public void setHasBeenValidatedForDisplay(boolean hasBeenValidatedForDisplay) {
		this.hasBeenValidatedForDisplay = hasBeenValidatedForDisplay;
	}

	@Override
	public String toString() {
		return "AlumnusUser [phoneNumber=" + phoneNumber + ", wantNotification=" + wantNotification + ", graduatedYear="
				+ graduatedYear + ", isValidated=" + isValidated + ", getFirstName()=" + getFirstName()
				+ ", getLastName()=" + getLastName() + ", getDescription()=" + getDescription() + ", getPictureLink()="
				+ getPictureLink() + ", getGroupName()=" + getGroupName() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
}
