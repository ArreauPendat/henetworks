package be.helha.aemt.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import be.helha.aemt.dao.AnnounceDAO;
import be.helha.aemt.entities.Announce;

@Stateless
@LocalBean
public class ManagementAnnounceEJB {

	@EJB
	private AnnounceDAO beanAnnounce;
	
	public boolean add(Announce announce) {
		return beanAnnounce.add(announce);
	}
	
	public List<Announce> selectAll() {
		return beanAnnounce.selectAll();
	}
	
}
