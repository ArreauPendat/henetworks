package be.helha.aemt.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import be.helha.aemt.dao.UserDAO;
import be.helha.aemt.entities.AlumnusUser;
import be.helha.aemt.entities.User;

@Stateless
@LocalBean
public class ManagementUserEJB {
	
	public ManagementUserEJB() {}
	
	@EJB
	private UserDAO beanUser;
	
	public String selectAll() {
		return beanUser.selectAll();
	}
	
	public boolean persistUser(User user) {
		return beanUser.add(user);
	}
	
	public List<AlumnusUser> selectAllNotValidated(){
		return beanUser.selectAllNotValidated();
	}
	
	public List<AlumnusUser> selectAllValidated(){
		return beanUser.selectAllValidated();
	}
	
	public List<AlumnusUser> selectAllCanBeDisplayed(){
		return beanUser.selectAllCanBeDisplayed();
	}
	
	public List<AlumnusUser> selectAllValidatedForDisplay(){
		return beanUser.selectAllValidatedForDisplay();
	}
	
	public User selectByMail(String mail) {
		return beanUser.findByMail(mail);
	}
	
	public boolean updateAlumnus(AlumnusUser user) {
		return beanUser.updateAlumnusInformation(user);
	}
	
	public boolean updateAlumnusValidated(AlumnusUser user) {
		return beanUser.updateAlumnusValidated(user);
	}
	
	public boolean deleteAlumnus(AlumnusUser user) {
		return beanUser.deleteAlumnus(user);
	}
	
	public int selectCountMembers() {
		return beanUser.selectCountMembers();
	}
	
	public int selectCountValidations() {
		return beanUser.selectCountValidations();
	}
}
