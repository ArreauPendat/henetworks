package be.helha.aemt.ejb;

import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import be.helha.aemt.dao.OfferDAO;
import be.helha.aemt.entities.AlumnusUser;
import be.helha.aemt.entities.InternshipOffer;
import be.helha.aemt.entities.JobOffer;
import be.helha.aemt.entities.Offer;

@Stateless
@LocalBean
public class ManagementOfferEJB {

	@EJB
	private OfferDAO beanOffer;
	
	public boolean add(Offer offer) {
		return beanOffer.add(offer);
	}

	public List<Offer> selectNotValidated() {
		return beanOffer.selectNotValidated();
	}
	
	public int countJobOffers() {
		return beanOffer.countJobOffers();
	}
	
	public int countInternshipOffers()
	{
		return beanOffer.countInternshipOffers();
	}
	
	public List<JobOffer> selectAllJobOffers(){
		return beanOffer.selectAllJobOffers();
	}
	
	public List<InternshipOffer> selectAllInternshipOffers(){
		return beanOffer.selectAllInternshipOffers();
	}
	
	public List<Offer> selectAllOffers(){
		return beanOffer.selectAllOffers();
	}

	public List<Offer> setValidated() {
		return beanOffer.selectValidated();
	}
	
	public boolean updateOfferValidated(Offer offer) {
		return beanOffer.updateOfferValidated(offer);
	}
	
	public boolean deleteOfferValidated(Offer offer) {
		return beanOffer.deleteOfferValidated(offer);
	}
	
	public boolean deleteOffer(Offer offer) {
		return beanOffer.deleteOffer(offer);
	}
	
	public int selectCountValidations() {
		return beanOffer.selectCountValidations();
	}
}
