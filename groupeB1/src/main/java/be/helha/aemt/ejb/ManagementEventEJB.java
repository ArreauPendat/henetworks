package be.helha.aemt.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import be.helha.aemt.dao.EventDAO;
import be.helha.aemt.entities.Event;

@Stateless
@LocalBean
public class ManagementEventEJB {
	
	@EJB
	private EventDAO beanEvent;
	
	public ManagementEventEJB() {}
	
	public boolean addEvent(Event event) {
		return beanEvent.addEvent(event);
	}
	
	public List<Event> selectAll() {
		return beanEvent.selectAll();
	}
	
}
