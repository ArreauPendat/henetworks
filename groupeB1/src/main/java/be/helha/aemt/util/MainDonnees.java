package be.helha.aemt.util;

import java.text.ParseException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import be.helha.aemt.entities.Address;
import be.helha.aemt.entities.AlumnusUser;
import be.helha.aemt.entities.Announce;
import be.helha.aemt.entities.Event;
import be.helha.aemt.entities.Group;
import be.helha.aemt.entities.InternshipOffer;
import be.helha.aemt.entities.JobOffer;
import be.helha.aemt.entities.Offer;
import be.helha.aemt.entities.Section;
public class MainDonnees {

	public static void main(String[] args) {
		
		AlumnusUser au1 = new AlumnusUser("test@gmail.com",Sha256Utility.convertToSha256("123"),"Mehdi","Ben Haddou","aze","pic",Group.ALUMNUS.toString(), Section.BUSINESS_COMPUTING.toString() ,"0476 14 52 95",true,2020,true, false, "");
		AlumnusUser au2 = new AlumnusUser("admin",Sha256Utility.convertToSha256("helha"),"Admin","Admin","zzz","zzz",Group.ADMINISTRATOR.toString(), Section.ACCOUNTING.toString(), "0476 14 52 95",true,2020,true, false, "");
		
		Address adr1 = new Address("Chauss�e de Binche", "159", 7000, "Mons");
		Event ev1 = null;
		try {
			ev1 = new Event("2020-01-01", "2020-05-08", 125, "Premier Evenement", "Le premier !", adr1);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		JobOffer job = new JobOffer("Premier Job", Section.ACCOUNTING.toString(), "Premi�re offre d'emploi", "2020-01-01", "HELHa", true, 2000.0, "Informaticien", 0);
		InternshipOffer internship = new InternshipOffer("Premier stage", Section.BUSINESS_COMPUTING.toString(), "Premi�re offre de stage", "2020-01-01", "HELHa", true, 90, true);
		
		Announce announce = new Announce("Annonce 1", "Ceci est une annonce");
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("groupeB1LOCAL");
		EntityManager em = emf.createEntityManager();
		EntityTransaction etr = em.getTransaction();
		
		etr.begin();
		/*
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		for(int i = 0; i < 5; i++) {
			String password = "PASSWORDD" + (i+1);
			byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			String sha256hex = new String(bytesToHex(hash));
			em.persist(new Utilisateur("LOGIN"+(i+1), sha256hex, "MAIL"+(i+1), "ancien"));
		}*/
		em.persist(au1);
		em.persist(au2);
		em.persist(adr1);
		if(ev1 != null) em.persist(ev1);
		em.persist(announce);
		em.persist(job);
		em.persist(internship);
		etr.commit();
		
	}

}
