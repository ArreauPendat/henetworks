package be.helha.aemt.control;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import be.helha.aemt.ejb.ManagementAnnounceEJB;
import be.helha.aemt.entities.Announce;
import be.helha.aemt.entities.User;

@RequestScoped
@Named
public class AnnounceControl implements Serializable {
	
	private String title;
	private String text;
	private String datePosted;
	
	@EJB
	private ManagementAnnounceEJB beanAnnounce;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getDatePosted() {
		return datePosted;
	}
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}
	
	public Announce createAnnounce() {
		return new Announce(title, text);
	}
	
	public String saveAnnounce() {
		beanAnnounce.add(createAnnounce());
		return "listAnnounce.xhtml";
	}
	
	public List<Announce> selectAllAnnounce() {
		return beanAnnounce.selectAll();
	}
	
}
