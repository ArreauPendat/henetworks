package be.helha.aemt.control;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;

import be.helha.aemt.ejb.ManagementEventEJB;
import be.helha.aemt.entities.Address;
import be.helha.aemt.entities.AlumnusUser;
import be.helha.aemt.entities.Event;

@RequestScoped
@Named
public class EventControl implements Serializable {
	
	private String name;
	private String startDate;
	private String endDate;
	private String description;
	private int placesAvailable;
	private Address address;
	
	//Address attribute
	private String town;
	private String street;
	private String number;
	private int postalCode;
	
	private int detailIndex;
	
	@EJB
	private ManagementEventEJB beanManagementEvent;
	
	private List<Event> events;
	
	public String doMoreDetails(String name) {
		Event evt = retrieveEventWithName(name);
		this.name = name;
		startDate = evt.getStartDate();
		endDate = evt.getEndDate();
		description = evt.getDescription();
		placesAvailable = evt.getPlacesAvailable();
		address = evt.getAddress();
		
		town = evt.getAddress().getTown();
		street = evt.getAddress().getStreet();
		number = evt.getAddress().getNumber();
		postalCode = evt.getAddress().getPostalCode();
		
		return "moreEventDetails.xhtml";
	}
	
	/////////////////////////////////////////////////////
	// Getters and setters
	/////////////////////////////////////////////////////
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPlacesAvailable() {
		return placesAvailable;
	}

	public void setPlacesAvailable(int placesAvailable) {
		this.placesAvailable = placesAvailable;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}
	
	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
	/////////////////////////////////////////////////////
	// METHODS
	/////////////////////////////////////////////////////

	public Event createEvent() {
		startDate = formatToCorrectDate(startDate);
		endDate = formatToCorrectDate(endDate);
		
		Event event = null;
		try {
			event = new Event(startDate, endDate, placesAvailable, name, description, createAddress());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return event;
	}
	
	public Address createAddress() {
		return new Address(street, number, postalCode, town);
	}
	
	private String formatToCorrectDate(String string) {
		String[] elements = string.split("/");
		return elements[2]+"-"+elements[1]+"-"+elements[0];
	}
	
	public String saveEvent() {
		beanManagementEvent.addEvent(createEvent());
		//TODO ramener � la page avec la liste des �v�nements
		return "listEvent.xhtml";
	}
	
	public void initEventList() {
		events = new ArrayList<>();
		events.addAll(beanManagementEvent.selectAll());
	}
	
	public List<Event> doSelectEventsForMainPage(){
		events = new ArrayList<>();
		events.addAll(beanManagementEvent.selectAll());
		List<Event> eventsForMainPage = new ArrayList<>();
		
		int nbEvents;
		if(events.size()<=5)
		{
			nbEvents = events.size();
		}
		else
		{
			nbEvents = 5;
		}
		
		for (int i = 0; i < nbEvents; i++) {
			eventsForMainPage.add(events.get(i));
		}
		return eventsForMainPage;
	}
	
	public Event retrieveEventWithName(String name) {
		for(Event evt : events) {
			if(evt.getName().equals(name)) {
				return evt;
			}
		}
		return null;
	}
	
	public String emptyTableMessage() {
		if(events.isEmpty())
		{
			return "Aucun �venement � afficher";
		}
		else
		{
			return "";
		}
	}
	
	////////////////////////////////////////////////////////////
	// PDF
	////////////////////////////////////////////////////////////
	
	public void createPDF() {
		String filePath = "." + File.separator + "events_pdf" + File.separator + name + "_evenement.pdf";
		Document document = new Document();
		try {
			File file = new File(filePath);
			file.getParentFile().mkdir();
			FileOutputStream output = new FileOutputStream(file);
			PdfWriter.getInstance(document, output);
			document.open();
			Font font = FontFactory.getFont(FontFactory.TIMES, 16, BaseColor.BLACK);
			Font title = FontFactory.getFont(FontFactory.HELVETICA, 22, BaseColor.BLUE);
			
			Phrase phrase = new Phrase();
			phrase.setFont(title);
			phrase.add(name);
			
			Phrase phrase1 = new Phrase();
			phrase1.setFont(font);
			phrase.add("\n" + description + "\n\ndu " + startDate + "\nau " + endDate + "\n" + placesAvailable + "places disponibles");
			
			document.add(phrase);
			document.add(phrase1);
			
			document.close();
			output.close();
		} catch (FileNotFoundException | DocumentException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void downloadPDF() throws IOException {
		createPDF();
		
		FileInputStream file = new FileInputStream("." + File.separator + "events_pdf" + File.separator + name + "_evenement.pdf"); 	    
		
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		
		ec.responseReset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
		ec.setResponseContentType("application/pdf"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ExternalContext#getMimeType() for auto-detection based on filename.
		ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + (name + "_evenement.pdf") + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
		
		OutputStream output = ec.getResponseOutputStream();
		// Now you can write the InputStream of the file to the above OutputStream the usual way.
		// ...
		byte[] buffer = new byte[1024];
		int length;
		while ((length = file.read(buffer)) > 0) {
			output.write(buffer, 0, length);
		}
		
		fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
		
	}
	
}
