package be.helha.aemt.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import be.helha.aemt.entities.Address;

public class AddressDAO {
	
//	@PersistenceContext(unitName="groupeB1JTA")
	private EntityManager em;
	
	public Address find(Address address) {
		String jpqlQuery = "SELECT ad FROM Address ad WHERE ad.number = :number AND ad.street = :street AND ad.town = :town";
		Query qlFind = em.createQuery(jpqlQuery);
		qlFind.setParameter("number", address.getNumber());
		qlFind.setParameter("street", address.getStreet());
		qlFind.setParameter("town", address.getTown());
		
		return (qlFind.getResultList().size() == 0)? null : (Address) qlFind.getResultList().get(0);
		
	}
	
	public void setEm(EntityManager em) {
		this.em = em;
	}
	
}
