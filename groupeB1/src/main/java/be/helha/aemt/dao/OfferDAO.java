package be.helha.aemt.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import be.helha.aemt.entities.AlumnusUser;
import be.helha.aemt.entities.InternshipOffer;
import be.helha.aemt.entities.JobOffer;
import be.helha.aemt.entities.Offer;

@Stateless
@LocalBean
public class OfferDAO {
	
	@PersistenceContext(unitName="groupeB1JTA")
	private EntityManager em;
	
	public boolean add(Offer offer) {
		em.persist(offer);
		return true;
	}

	public List<Offer> selectNotValidated() {
		String jpqlQuery = "SELECT obj FROM Offer obj WHERE obj.validated = 0";
		Query qrValidation = em.createQuery(jpqlQuery);
		return qrValidation.getResultList();
	}

	public List<Offer> selectValidated() {
		String jpqlQuery = "SELECT obj FROM Offer obj WHERE obj.validated = 1";
		Query qrValidation = em.createQuery(jpqlQuery);
		return qrValidation.getResultList();
	}
	
	public List<JobOffer> selectAllJobOffers(){
		Query qSelectAllJobOffers = em.createQuery("SELECT obj FROM JobOffer obj where obj.validated = 1");
		List<JobOffer> list = qSelectAllJobOffers.getResultList();
		return list;
		
	}
	
	public int countJobOffers() {
		List<JobOffer> list = selectAllJobOffers();
		return list.size();
	}
	
	public List<InternshipOffer> selectAllInternshipOffers(){
		Query qSelectAllInternshipOffers = em.createQuery("SELECT obj FROM InternshipOffer obj where obj.validated = 1");
		List<InternshipOffer> list = qSelectAllInternshipOffers.getResultList();
		return list;
	}
	
	public int countInternshipOffers() {
		List<InternshipOffer> list = selectAllInternshipOffers();
		return list.size();
	}
	
	public List<Offer> selectAllOffers(){
		Query qSelectAllOffers = em.createQuery("SELECT obj FROM Offer obj");
		List<Offer> list = qSelectAllOffers.getResultList();
		return list;
	}
	
	public boolean updateOfferValidated(Offer offer) {
		String jpqlQuery = "UPDATE Offer offer SET "
				+ "offer.validated = :validated "
				+" WHERE offer.id = :id";
		Query updateQuery = em.createQuery(jpqlQuery);
		updateQuery.setParameter("id", offer.getId());
		updateQuery.setParameter("validated", offer.isValidated());
		return updateQuery.executeUpdate() > 0;
	}
	
	public boolean deleteOfferValidated(Offer offer) {
		String jpqlQuery = "DELETE FROM Offer offer"
				+" WHERE offer.id = :id AND offer.validated = 0";
		Query updateQuery = em.createQuery(jpqlQuery);
		updateQuery.setParameter("id", offer.getId());
		return updateQuery.executeUpdate() > 0;
	}
	
	public boolean deleteOffer(Offer offer) {
		String jpqlQuery = "DELETE FROM Offer offer"
				+" WHERE offer.id = :id";
		Query updateQuery = em.createQuery(jpqlQuery);
		updateQuery.setParameter("id", offer.getId());
		return updateQuery.executeUpdate() > 0;
	}
	public int selectCountValidations() {
		List<Offer> list = selectNotValidated();
		return list.size();
	}
}
