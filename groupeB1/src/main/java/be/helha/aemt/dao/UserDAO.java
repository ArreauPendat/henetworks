package be.helha.aemt.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import be.helha.aemt.entities.AlumnusUser;
import be.helha.aemt.entities.Group;
import be.helha.aemt.entities.User;

@LocalBean
@Stateless
public class UserDAO {
	
	@PersistenceContext(unitName="groupeB1JTA")
	private EntityManager em;
	
	public String selectAll() {
		Query qselectall = em.createQuery("SELECT obj FROM User obj");
		return qselectall.getResultList().toString();
	}

	public boolean add(User user) {
		if(findByMail(user.getMail()) != null) return false;
		em.persist(user);
		return true;
	}

	public User findByMail(String mail) {
		String jpqlQuery = "SELECT obj FROM User obj WHERE obj.mail=:mail";
		Query qlFindMail = em.createQuery(jpqlQuery);
		qlFindMail.setParameter("mail", mail);
		List<User> elements = qlFindMail.getResultList();
		return (elements.size() == 0)?null:elements.get(0);
	}
	
	public List<AlumnusUser> selectAllNotValidated() {
		Query qSelectAllNotValidated = em.createQuery("SELECT obj FROM AlumnusUser obj where obj.isValidated = 0");
		List<AlumnusUser> list = qSelectAllNotValidated.getResultList();
		return list;
	}
	
	public List<AlumnusUser> selectAllValidated() {
		Query qSelectAllNotValidated = em.createQuery("SELECT obj FROM AlumnusUser obj where obj.isValidated = 1");
		List<AlumnusUser> list = qSelectAllNotValidated.getResultList();
		return list;
	}
	
	public List<AlumnusUser> selectAllCanBeDisplayed() {
		Query qSelectAllCanBeDisplayed = em.createQuery("SELECT obj FROM AlumnusUser obj where obj.canDisplayed = 1");
		List<AlumnusUser> list = qSelectAllCanBeDisplayed.getResultList();
		return list;
	}
	
	public List<AlumnusUser> selectAllValidatedForDisplay() {
		Query qSelectAllValidatedForDisplay = em.createQuery("SELECT obj FROM AlumnusUser obj where obj.hasBeenValidatedForDisplay = 1");
		List<AlumnusUser> list = qSelectAllValidatedForDisplay.getResultList();
		return list;
	}
	
	public boolean updateAlumnusInformation(AlumnusUser user) {
		String jpqlQuery = "UPDATE AlumnusUser alumnus SET "
				+ "alumnus.firstName = :firstname, "
				+ "alumnus.lastName = :lastname, "
				+ "alumnus.phoneNumber = :phonenumber, "
				+ "alumnus.description = :description, "
				+ "alumnus.wantNotification = :wantnotif, "
				+ "alumnus.hasBeenValidatedForDisplay = :isDisplayed, "
				+ "alumnus.canDisplayed = :canDisplayed, "
				+ "alumnus.testimony = :testimony "
				+" WHERE alumnus.mail = :mail";
		Query updateQuery = em.createQuery(jpqlQuery);
		updateQuery.setParameter("mail", user.getMail());
		updateQuery.setParameter("firstname", user.getFirstName());
		updateQuery.setParameter("lastname", user.getLastName());
		updateQuery.setParameter("wantnotif", user.isWantNotification());
		updateQuery.setParameter("phonenumber", user.getPhoneNumber());
		updateQuery.setParameter("description", user.getDescription());
		updateQuery.setParameter("isDisplayed", user.isHasBeenValidatedForDisplay());
		updateQuery.setParameter("canDisplayed", user.isCanDisplayed());
		updateQuery.setParameter("testimony", user.getTestimony());
		return updateQuery.executeUpdate() > 0;
	}
	
	public boolean updateAlumnusValidated(AlumnusUser user) {
		String jpqlQuery = "UPDATE AlumnusUser alumnus SET "
				+ "alumnus.isValidated = :isvalidated "
				+" WHERE alumnus.mail = :mail";
		Query updateQuery = em.createQuery(jpqlQuery);
		updateQuery.setParameter("mail", user.getMail());
		updateQuery.setParameter("isvalidated", user.isIsValidated());
		if (! (updateQuery.executeUpdate() > 0)) return false;
		jpqlQuery = "UPDATE User user SET user.groupName = 'ancien' WHERE user.mail = :mail";
		Query updateUserQuery = em.createQuery(jpqlQuery);
		updateUserQuery.setParameter("mail", user.getMail());
		return updateUserQuery.executeUpdate() > 0;
	}
	
	public boolean deleteAlumnus(AlumnusUser user) {
		String jpqlQuery = "DELETE FROM AlumnusUser alumnus"
				+" WHERE alumnus.mail = :mail";
		Query updateQuery = em.createQuery(jpqlQuery);
		updateQuery.setParameter("mail", user.getMail());
		return updateQuery.executeUpdate() > 0;
	}
	
	public int selectCountMembers() {
		List<AlumnusUser> list = selectAllValidated();
		return list.size();
	}
	
	public int selectCountValidations() {
		List<AlumnusUser> list = selectAllNotValidated();
		return list.size();
	}
}
