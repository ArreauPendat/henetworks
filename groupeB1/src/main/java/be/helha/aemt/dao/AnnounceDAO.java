package be.helha.aemt.dao;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import be.helha.aemt.entities.Announce;

@Stateless
@LocalBean
public class AnnounceDAO {
	
	@PersistenceContext(unitName="groupeB1JTA")
	private EntityManager em;
	
	public boolean add(Announce announce) {
		em.persist(announce);
		return true;
	}
	
	public List<Announce> selectAll() {
		String jpqlQuery = "SELECT obj FROM Announce obj";
		Query query = em.createQuery(jpqlQuery);
		return query.getResultList();
	}
	
}
